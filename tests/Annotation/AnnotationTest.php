<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */

namespace LP\PropertyBinder\Tests\Annotation;

use LP\PropertyBinder\Annotation\Annotation;
use PHPUnit\Framework\TestCase;

class AnnotationTest extends TestCase {

    public function testAbstractAnnotation() {

        $this->markTestSkipped();
    }
}