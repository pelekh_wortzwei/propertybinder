<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
namespace LP\PropertyBinder\Tests;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\ArrayCollection;
use LP\PropertyBinder\Metadata\Driver\AnnotationDriver;
use LP\PropertyBinder\Metadata\Driver\YamlDriver;
use LP\PropertyBinder\PropertyBinder;
use LP\PropertyBinder\PropertyBinderBuilder;
use LP\PropertyBinder\Tests\TestClass\ChildTestClass;
use LP\PropertyBinder\Tests\TestClass\RootTestClass;
use Metadata\Driver\FileLocator;
use PHPUnit\Framework\TestCase;

class PropertyBinderTest extends TestCase {

    protected function setUp() {
        parent::setUp();
        $this->propertyBinder = DefaultPropertyBinderFactory::create();
    }

    public function testBindSimpleData() {
        $propertyBinder = $this->createPropertyBinder();
        $testObj = $this->createTestClass();

        $testData = [
            'foo' => 'foo1',
            'bar' => 'bar1',
            'number' => 7,
            'setBySetter' => 'Hello',
            'setByCustomSetter' => 100,
        ];

        $propertyBinder->bind($testObj, $testData);
        $this->assertSame($testData['foo'], $testObj->getFoo());
        $this->assertNull($testObj->getBar());
        $this->assertSame($testData['number'], $testObj->getNumber());
        $this->assertSame($testData['setBySetter'], $testObj->getSetBySetter());
        $this->assertTrue($testObj->getWasSetBySetter());
        $this->assertSame($testData['setByCustomSetter'], $testObj->getByCustomSetter());
    }

    public function testBindGroups() {
        $propertyBinder = $this->createPropertyBinder();
        $testObj = $this->createTestClass();

        $testData = [
            'foo' => 'foo1',
            'bar' => 'bar1',
            'group1' => 'Gruppe 1'
        ];
        $propertyBinder->bind($testObj, $testData, ['testgroup1']);
        $this->assertNull($testObj->getFoo());
        $this->assertNull($testObj->getBar());
        $this->assertSame($testData['group1'], $testObj->getGroup1());
    }

    public function testDateTimeBinding() {
        $propertyBinder = $this->createPropertyBinder();
        $testObj = $this->createTestClass();

        $testData = [
            'date1' => '2000-11-15',
            'date2' => '2000-11-15 12:00:30'
        ];
        $propertyBinder->bind($testObj, $testData);

        $this->assertInstanceOf(\DateTime::class, $testObj->getDate1());
        $this->assertSame($testData['date1'], $testObj->getDate1()->format('Y-m-d'));
        $this->assertInstanceOf(\DateTime::class, $testObj->getDate2());
        $this->assertSame($testData['date2'], $testObj->getDate2()->format('Y-m-d H:i:s'));
    }

    public function testObjectBinding() {
        $propertyBinder = $this->createPropertyBinder();
        $testObj = $this->createTestClass();
        $testData = [
            'foo' => 'fooooo',
            'bar' => 'baaaaar',
            'object' => [
                'name' => 'Hello'
            ]
        ];
        $propertyBinder->bind($testObj, $testData);
        $this->assertSame($testData['object']['name'], $testObj->getObject()->getName());
    }

    public function testSimpleDataCollectionBinding() {
        $propertyBinder = $this->createPropertyBinder();

        $testObj = $this->createTestClass();
        $testData = [
            'array' => ['test1', 'test2', 'test3']
        ];
        $propertyBinder->bind($testObj, $testData);
        $this->assertSame($testData['array'], $testObj->getArray());

        $testObj = $this->createTestClass();
        $testData = [
            'collection' => ['test1', 'test2', 'test3']
        ];
        $propertyBinder->bind($testObj, $testData);

        $this->assertInstanceOf(ArrayCollection::class, $testObj->getCollection());
        $this->assertSame($testData['collection'], $testObj->getCollection()->toArray());
    }

    public function testObjectCollectionBinding() {
        $propertyBinder = $this->createPropertyBinder();

        $testObj = $this->createTestClass();
        $testData = [
            'arrayObjects' => [
                ['name' => 'Childclass1', 'number' => 10],
                ['name' => 'Childclass2', 'number' => 20],
                ['name' => 'Childclass3', 'number' => 30],
            ]
        ];
        $propertyBinder->bind($testObj, $testData);
        $this->assertCount(count($testData['arrayObjects']), $testObj->getArrayObjects());
        foreach($testData['arrayObjects'] as $index => $values) {
            $this->assertSame($values['name'], $testObj->getArrayObjects()[$index]->getName());
            $this->assertSame($values['number'], $testObj->getArrayObjects()[$index]->getNumber());
        }

        $testObj = $this->createTestClass();
        $testData = [
            'collectionObjects' => [
                ['name' => 'Childclass1', 'number' => 10],
                ['name' => 'Childclass2', 'number' => 20],
                ['name' => 'Childclass3', 'number' => 30],
            ]
        ];
        $propertyBinder->bind($testObj, $testData);
        $this->assertInstanceOf(ArrayCollection::class, $testObj->getCollectionObjects());
        $this->assertCount(count($testData['collectionObjects']), $testObj->getCollectionObjects()->toArray());
        foreach($testData['collectionObjects'] as $index => $values) {
            $this->assertSame($values['name'], $testObj->getCollectionObjects()[$index]->getName());
            $this->assertSame($values['number'], $testObj->getCollectionObjects()[$index]->getNumber());
        }
    }

    public function testNestedCollectionBinding() {
        $propertyBinder = $this->createPropertyBinder();
        $testObj = $this->createTestClass();
        $testData = [
            'object' => [
                    'name' => 'ChildClass1',
                    'number' => 10,
                    'child' => [
                        'name' => 'ChildChildClass1',
                        'number' => 1000
                    ],
                    'children' => [
                        ['name' => 'ChildChildClass1', 'number' => 10],
                        ['name' => 'ChildChildClass2', 'number' => 20],
                        ['name' => 'ChildChildClass3', 'number' => 30],
                    ]
            ]
        ];
        $propertyBinder->bind($testObj, $testData);

        $this->assertInstanceOf(ChildTestClass::class, $testObj->getObject()->child);
        $this->assertSame($testData['object']['child']['name'], $testObj->getObject()->child->getName());
        $this->assertSame($testData['object']['child']['number'], $testObj->getObject()->child->getNumber());

        $this->assertInstanceOf(ArrayCollection::class, $testObj->getObject()->children);
        $this->assertCount(count($testData['object']['children']), $testObj->getObject()->children->toArray());
        foreach($testData['object']['children'] as $index => $child) {
            $this->assertSame($testData['object']['children'][$index]['name'], $testObj->getObject()->children[$index]->getName());
            $this->assertSame($testData['object']['children'][$index]['number'], $testObj->getObject()->children[$index]->getNumber());
        }

    }

    private function createTestClass() {
        return new RootTestClass();
    }

    /**
     * @return PropertyBinder
     */
    private function createPropertyBinder() {

        $annotationDriver = new AnnotationDriver(new AnnotationReader());
        $fileLocator = new FileLocator([]);
        $yamlDriver = new YamlDriver($fileLocator);

        $propertyBinderBuilder = new PropertyBinderBuilder();
        $propertyBinderBuilder
            ->addDriver($annotationDriver)
            ->addDriver($yamlDriver);
        return $propertyBinderBuilder->build();
    }
}