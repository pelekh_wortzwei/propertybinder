<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */

namespace LP\PropertyBinder\Tests\TestClass;

use Doctrine\Common\Collections\ArrayCollection;
use LP\PropertyBinder\Annotation\BindMany;
use LP\PropertyBinder\Annotation\BindOne;

class RootTestClass {
    /**
     * @BindOne(type="string")
     * @var string
     */
    private $foo;
    private $bar;
    /**
     * @BindOne(type="integer")
     * @var int
     */
    private $number;

    /**
     * @BindOne(type="string", useSetter=true)
     */
    private $setBySetter;

    /**
     * @BindOne(type="integer", useSetter=true, setterName="myCustomSetter")
     */
    private $setByCustomSetter;

    /**
     * @var boolean
     */
    private $wasSetBySetter = false;

    /**
     * @BindOne(type="string", groups={"testgroup1"})
     */
    private $group1;

    /**
     * @BindOne(type="\DateTime")
     */
    private $date1;

    /**
     * @BindOne(type="\DateTime")
     */
    private $date2;

    /**
     * @BindOne(type="LP\PropertyBinder\Tests\TestClass\ChildTestClass")
     */
    private $object;

    /**
     * @BindMany(type="string", collectionType="array")
     */
    private $array;

    /**
     * @BindMany(type="string", collectionType="collection")
     * @var ArrayCollection
     */
    private $collection;

    /**
     * @BindMany(type="LP\PropertyBinder\Tests\TestClass\ChildTestClass", collectionType="array")
     * @var array
     */
    private $arrayObjects;

    /**
     * @BindMany(type="LP\PropertyBinder\Tests\TestClass\ChildTestClass", collectionType="collection")
     * @var ArrayCollection
     */
    private $collectionObjects;

    public function __construct() {
        $this->object = new ChildTestClass();
    }

    public function setSetBySetter(string $val) {
        $this->setBySetter = $val;
        $this->wasSetBySetter = true;
    }
    public function myCustomSetter(int $val) {
        $this->setByCustomSetter = $val;
    }
    public function getFoo() { return $this->foo;}
    public function getGroup1() { return $this->group1;}
    public function getBar() {return $this->bar;}
    public function getNumber() {return $this->number;}
    public function getSetBySetter() {return $this->setBySetter;}
    public function getWasSetBySetter() {return $this->wasSetBySetter;}
    public function getByCustomSetter() {return $this->setByCustomSetter;}
    public function getDate1() {return $this->date1;}
    public function getDate2() {return $this->date2;}
    public function getObject() {return $this->object;}
    public function getArray() {return $this->array;}
    public function getCollection() {return $this->collection;}
    public function getArrayObjects() {return $this->arrayObjects;}
    public function getCollectionObjects() {return $this->collectionObjects;}
}
