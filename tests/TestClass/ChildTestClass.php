<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */

namespace LP\PropertyBinder\Tests\TestClass;

use Doctrine\Common\Collections\ArrayCollection;
use LP\PropertyBinder\Annotation\BindMany;
use LP\PropertyBinder\Annotation\BindOne;

class ChildTestClass {
    /**
     * @BindOne(type="string")
     */
    private $name;

    /**
     * @BindOne(type="int")
     */
    private $number;

    /**
     * @var ChildTestClass
     * @BindOne(type="LP\PropertyBinder\Tests\TestClass\ChildTestClass")
     */
    public $child;

    /**
     * @var ArrayCollection
     * @BindMany(type="LP\PropertyBinder\Tests\TestClass\ChildTestClass", collectionType="collection")
     */
    public $children;

    public function getName() { return $this->name;}
    public function getNumber() { return $this->number;}
    public function getChild() { return $this->child;}
    public function getChildren() { return $this->child;}
}