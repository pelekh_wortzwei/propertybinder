<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
namespace LP\PropertyBinder\Tests\Metadata;

use LP\PropertyBinder\Metadata\PropertyMetadata;
use PHPUnit\Framework\TestCase;

class PropertyMetaDataTest extends TestCase {

    public function testIsSingleOrCollectionType() {
        $testClass = $this->createTestClass();
        $propertyMetadata = new PropertyMetadata(get_class($testClass), 'foo');

        $this->assertFalse($propertyMetadata->isCollectionType());
        $this->assertFalse($propertyMetadata->hasCollectionBinding());

        $propertyMetadata->collectionType = PropertyMetadata::ARRAY;
        $this->assertTrue($propertyMetadata->isArrayType());
        $this->assertTrue($propertyMetadata->hasCollectionBinding());

        $propertyMetadata->collectionType = PropertyMetadata::COLLECTION;
        $this->assertTrue($propertyMetadata->isCollectionType());
        $this->assertTrue($propertyMetadata->hasCollectionBinding());
    }

    public function testSetValue() {
        $testClass = $this->createTestClass();
        $propertyMetadata = new PropertyMetadata(get_class($testClass), 'foo');
        $propertyMetadata->setValue($testClass, 'foo');
        $this->assertSame('foo', $testClass->foo);

        $propertyMetadata->useSetter = true;
        $propertyMetadata->setValue($testClass, 'foo');
        $this->assertSame('setFoo', $testClass->foo);

        $propertyMetadata->setterName = 'setAlternativeFoo';
        $propertyMetadata->setValue($testClass, 'foo');
        $this->assertSame('setAlternativeFoo', $testClass->foo);
    }

    public function testGetValue() {
        $testClass = $this->createTestClass();
        $propertyMetadata = new PropertyMetadata(get_class($testClass), 'foo');
        $value = $propertyMetadata->getValue($testClass);
        $this->assertNull($value);

        $propertyMetadata->useGetter = true;
        $value = $propertyMetadata->getValue($testClass);
        $this->assertSame('getFoo', $value);

        $propertyMetadata->getterName = 'getAlternativeFoo';
        $value = $propertyMetadata->getValue($testClass);
        $this->assertSame('getAlternativeFoo', $value);
    }

    private function createTestClass() {
        return new class {
            public $foo;

            public function setFoo($foo) {
                $this->foo = 'setFoo';
            }

            public function setAlternativeFoo($foo) {
                $this->foo = 'setAlternativeFoo';
            }

            public function getFoo() {
                return 'getFoo';
            }

            public function getAlternativeFoo() {
                return 'getAlternativeFoo';
            }
        };
    }
}