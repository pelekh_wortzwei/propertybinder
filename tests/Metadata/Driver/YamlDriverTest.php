<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
namespace LP\PropertyBinder\Tests\Metadata\Driver;

use LP\PropertyBinder\Metadata\Driver\YamlDriver;
use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\Tests\TestClass\ChildTestClass;
use LP\PropertyBinder\Tests\TestClass\RootTestClass;
use Metadata\Driver\FileLocator;
use PHPUnit\Framework\TestCase;

class YamlDriverTest extends TestCase {

    public function testLoadYamlData() {
        $fileLocator = new FileLocator([
            'LP\PropertyBinder\Tests' => __DIR__ . '/../../Resources'
        ]);
        $driver = new YamlDriver($fileLocator);
        $classMetadata = $driver->loadMetadataForClass(new \ReflectionClass(RootTestClass::class));

        $this->assertCount(4, $classMetadata->propertyMetadata);

        $this->assertArrayHasKey('foo', $classMetadata->propertyMetadata);
        // default fields
        /** @var PropertyMetadata $fooMetadata */
        $fooMetadata = $classMetadata->propertyMetadata['foo'];
        $this->assertSame('string', $fooMetadata->type);
        $this->assertIsArray($fooMetadata->groups);
        $this->assertCount(0, $fooMetadata->groups);
        $this->assertFalse($fooMetadata->useSetter);
        $this->assertFalse($fooMetadata->useGetter);
        $this->assertNull($fooMetadata->arrayKey);
        $this->assertNull($fooMetadata->setterName);
        $this->assertNull($fooMetadata->getterName);
        $this->assertNull($fooMetadata->collectionType);

        $this->assertArrayHasKey('number', $classMetadata->propertyMetadata);
        /** @var PropertyMetadata $numberMetadata */
        $numberMetadata = $classMetadata->propertyMetadata['number'];
        $this->assertSame('integer', $numberMetadata->type);
        $this->assertTrue(in_array('group1', $numberMetadata->groups));
        $this->assertTrue($numberMetadata->useSetter);
        $this->assertTrue($numberMetadata->useGetter);
        $this->assertSame('number_x', $numberMetadata->arrayKey);
        $this->assertSame('setterName', $numberMetadata->setterName);
        $this->assertSame('getterName', $numberMetadata->getterName);
        $this->assertNull($numberMetadata->collectionType);

        $this->assertArrayHasKey('bar', $classMetadata->propertyMetadata);
        /** @var PropertyMetadata $barMetadata */
        $barMetadata = $classMetadata->propertyMetadata['bar'];
        $this->assertSame(\DateTime::class, ltrim($barMetadata->type, '\\'));
        $this->assertSame($barMetadata->collectionType, PropertyMetadata::ARRAY);

        $this->assertArrayHasKey('collection', $classMetadata->propertyMetadata);
        /** @var PropertyMetadata $collectionMetadata */
        $collectionMetadata = $classMetadata->propertyMetadata['collection'];
        $this->assertSame(ChildTestClass::class, ltrim($collectionMetadata->type, '\\'));
        $this->assertSame($collectionMetadata->collectionType, PropertyMetadata::COLLECTION);
    }
}