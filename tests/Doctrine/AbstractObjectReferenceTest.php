<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Tests\Doctrine;

use Doctrine\Persistence\Mapping\ClassMetadata;
use LP\PropertyBinder\Doctrine\AbstractObjectReference;
use LP\PropertyBinder\Tests\AssertException;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractObjectReferenceTest
 * @package LP\PropertyBinder\Tests\Doctrine
 */
class AbstractObjectReferenceTest extends TestCase {

    use AssertException;

    /**
     * @var AbstractObjectReference
     */
    private $abstractObjectReferenceMock;

    protected function setUp() {
        $this->abstractObjectReferenceMock = $this->getMockForAbstractClass(AbstractObjectReference::class);
        parent::setUp();
    }


    public function testGetIdentifierName() {
        $classMetadataMock = $this->createMock(ClassMetadata::class);
        $classMetadataMock
            ->expects($this->any())
            ->method('getIdentifier')
            ->will($this->returnValue(['123']));

        $result = $this->abstractObjectReferenceMock->getIdentifierFieldName($classMetadataMock);
        $this->assertSame('123', $result);

        $classMetadataMock = $this->createMock(ClassMetadata::class);
        $classMetadataMock
            ->expects($this->any())->method('getIdentifier')->will($this->returnValue(['123', '123']));
        $classMetadataMock
            ->expects($this->any())->method('getName')->will($this->returnValue('object'));

        $abstractObjectReferenceMock = $this->abstractObjectReferenceMock;
        $this->assertException(\Exception::class, function() use($abstractObjectReferenceMock, $classMetadataMock) {
            $abstractObjectReferenceMock->getIdentifierFieldName($classMetadataMock);
            return;
        });
    }

    public function testGetIdentifierType() {
        $classMetadataMock = $this->createMock(ClassMetadata::class);
        $classMetadataMock
            ->expects($this->any())
            ->method('getTypeOfField')
            ->with('test')
            ->will($this->returnValue('string'));

        $result = $this->abstractObjectReferenceMock->getIdentifierFieldType($classMetadataMock, 'test');
        $this->assertSame('string', $result);

        $classMetadataMock = $this->createMock(ClassMetadata::class);
        $classMetadataMock
            ->expects($this->any())->method('getTypeOfField')->will($this->returnValue(null));
        $classMetadataMock
            ->expects($this->any())->method('getName')->will($this->returnValue('object'));

        $abstractObjectReferenceMock = $this->abstractObjectReferenceMock;
        $this->assertException(\Exception::class, function() use($abstractObjectReferenceMock, $classMetadataMock) {
            $abstractObjectReferenceMock->getIdentifierFieldType($classMetadataMock, 'test');
            return;
        });
    }

    public function testGetReferenceValue() {
        $data = [
            'id' => 'hurra'
        ];
        $result = $this->abstractObjectReferenceMock->getReferenceValue($data, 'id', 'string');
        $this->assertSame($result, $data['id']);

        $result = $this->abstractObjectReferenceMock->getReferenceValue($data, 'id', 'int');
        $this->assertNull($result);

        $data = 100;
        $result = $this->abstractObjectReferenceMock->getReferenceValue($data, 'id', 'integer');
        $this->assertSame($data, $result);

        $data = '123a';
        $result = $this->abstractObjectReferenceMock->getReferenceValue($data, 'id', 'int');
        $this->assertNull($result);
    }
}