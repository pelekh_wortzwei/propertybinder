<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */

use Doctrine\Common\Annotations\AnnotationRegistry;

(static function () {
    if (!is_file($autoloadFile = __DIR__ . '/../vendor/autoload.php')) {
        throw new RuntimeException('Did not find vendor/autoload.php. Did you run "composer install --dev"?');
    }

    $loader = require $autoloadFile;
    $loader->add('LP\PropertyBinder\Tests', __DIR__);
    AnnotationRegistry::registerLoader('class_exists');
})();
