<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */

namespace LP\PropertyBinder\Tests\Handler\Binding;

use LP\PropertyBinder\Handler\Binding\SimpleDataBinding;

class SimpleDataBindingTest extends BindingTest {

    public function testSupports() {
        $binding = new SimpleDataBinding();

        foreach(['int', 'integer', 'float', 'string', 'bool', 'boolean'] as $type) {
            $this->assertTrue($binding->supports($type));
            $this->assertFalse($binding->isCollectionBinding());
        }
    }

    public function testBindingInteger() {
        $Binding = new SimpleDataBinding();

        $propertyMeta = $this->createPropertyMeta('foo', 'integer');

        foreach(['6' => 6, 'a' => 0, 1000 => 1000, null => 0] as $source => $dest) {
            $integer = $Binding->bind($source, null, $propertyMeta, $this->propertyBinder);
            $this->assertIsInt($integer);
            $this->assertSame($dest, $integer);
        }
    }

    public function testBindingString() {
        $Binding = new SimpleDataBinding();
        $propertyMeta = $this->createPropertyMeta('foo', 'string');

        foreach(['6' => '6', 'a' => 'a', 1000 => '1000', null => ''] as $source => $dest) {
            $string = $Binding->bind($source, null, $propertyMeta, $this->propertyBinder);
            $this->assertIsString($string);
            $this->assertSame($dest, $string);
        }
    }

    public function testBindingBoolean() {
        $Binding = new SimpleDataBinding();
        $propertyMeta = $this->createPropertyMeta('foo', 'boolean');

        foreach(['1' => true, '0' => false, 1 => true, 0 => false, null => false, true => true, false => false] as $source => $dest) {
            $boolean = $Binding->bind($source, null, $propertyMeta, $this->propertyBinder);
            $this->assertIsBool($boolean);
            $this->assertSame($dest, $boolean);
        }
    }

    public function testBindingFloat() {
        $Binding = new SimpleDataBinding();
        $propertyMeta = $this->createPropertyMeta('foo', 'float');

        foreach(['2' => 2.0, 4 => 4.0, 10.0 => 10.0] as $source => $dest) {
            $float = $Binding->bind($source, null, $propertyMeta, $this->propertyBinder);
            $this->assertIsFloat($float);
            $this->assertSame($dest, $float);
        }
    }
}