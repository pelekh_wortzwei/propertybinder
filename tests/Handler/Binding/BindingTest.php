<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Tests\Handler\Binding;

use Doctrine\Common\Annotations\AnnotationReader;
use LP\PropertyBinder\Doctrine\DoctrineObjectReference;
use LP\PropertyBinder\Metadata\Driver\AnnotationDriver;
use LP\PropertyBinder\Metadata\Driver\YamlDriver;
use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\PropertyBinder;
use LP\PropertyBinder\PropertyBinderBuilder;
use LP\PropertyBinder\Tests\TestClass\RootTestClass;
use Metadata\Driver\FileLocator;
use PHPUnit\Framework\TestCase;

abstract class BindingTest extends TestCase {

    /**
     * @var PropertyBinder
     */
    protected $propertyBinder;

    protected function setUp() {
        parent::setUp();
        $this->propertyBinder = $this->createPropertyBinder();
    }

    protected function createPropertyBinder(DoctrineObjectReference $doctrineObjectReference = null):PropertyBinder {
        $annotationDriver = new AnnotationDriver(new AnnotationReader());
        $fileLocator = new FileLocator([]);
        $yamlDriver = new YamlDriver($fileLocator);

        $propertyBinderBuilder = new PropertyBinderBuilder();
        if($doctrineObjectReference != null) {
            $propertyBinderBuilder->setDoctrineObjectReference($doctrineObjectReference);
        }
        $propertyBinderBuilder
            ->addDriver($annotationDriver)
            ->addDriver($yamlDriver);
        return $propertyBinderBuilder->build();
    }

    /**
     * @param string      $propertyName
     *
     * @param string|null $type
     *
     * @return PropertyMetadata
     */
    protected function createPropertyMeta(string $propertyName, ?string $type = null):PropertyMetadata {
        $propertyMetadata = new PropertyMetadata(RootTestClass::class, $propertyName);
        $propertyMetadata->type = $type;
        return $propertyMetadata;
    }
}