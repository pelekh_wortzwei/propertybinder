<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
namespace LP\PropertyBinder\Tests\Handler\Binding;

use LP\PropertyBinder\Handler\Binding\DateTimeBinding;

class DateTimeBindingTest extends BindingTest {

    public function testSupports() {
        $dateTimeBinding = new DateTimeBinding();
        $this->assertTrue($dateTimeBinding->supports('\DateTime'));
        $this->assertFalse($dateTimeBinding->isCollectionBinding());
    }

    public function testBinding() {
        $dateString = '2001-11-24';
        $dateTimeBinding = new DateTimeBinding();

        $propertyMeta = $this->createPropertyMeta('foo');

        /** @var \DateTime $date */
        $date = $dateTimeBinding->bind($dateString, null, $propertyMeta, $this->propertyBinder);
        $this->assertInstanceOf(\DateTime::class, $date);
        $this->assertSame($date->format('Y-m-d'), $dateString);

        $dateTimeString = '2001-11-24 12:04:34';
        /** @var \DateTime $date */
        $date = $dateTimeBinding->bind($dateTimeString, null, $propertyMeta, $this->propertyBinder);
        $this->assertInstanceOf(\DateTime::class, $date);
        $this->assertSame($date->format('Y-m-d H:i:s'), $dateTimeString);

        $noDate = null;
        $date = $dateTimeBinding->bind($noDate, null, $propertyMeta, $this->propertyBinder);
        $this->assertNull($date);
    }
}