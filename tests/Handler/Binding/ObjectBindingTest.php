<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Tests\Handler\Binding;

use LP\PropertyBinder\Handler\Binding\ObjectBinding;
use LP\PropertyBinder\Tests\TestClass\ChildTestClass;

class ObjectBindingTest extends BindingTest {

    public function testObjectBinding() {
        $objectBinding = new ObjectBinding();
        $propertyMeta = $this->createPropertyMeta('object', ChildTestClass::class);
        $this->assertTrue($objectBinding->supports(ChildTestClass::class));
        $this->assertFalse($objectBinding->isCollectionBinding());

        $testData = [
            'name' => 'Hello',
            'number' => 5
        ];

        $value = $objectBinding->bind($testData, null, $propertyMeta, $this->propertyBinder);
        $this->assertSame('Hello', $value->getName());
        $this->assertSame(5, $value->getNumber());
    }

    public function testDoctrineObjectBinding() {

        $this->markTestSkipped();
    }
}