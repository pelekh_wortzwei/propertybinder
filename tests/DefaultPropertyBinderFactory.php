<?php

/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Tests;

use Doctrine\Common\Annotations\AnnotationReader;
use LP\PropertyBinder\Doctrine\DoctrineObjectReference;
use LP\PropertyBinder\Metadata\Driver\AnnotationDriver;
use LP\PropertyBinder\Metadata\Driver\YamlDriver;
use LP\PropertyBinder\PropertyBinder;
use LP\PropertyBinder\PropertyBinderBuilder;
use Metadata\Driver\FileLocator;

/**
 * Class DefaultPropertyBinderFactory
 * @package LP\PropertyBinder\Tests
 */
class DefaultPropertyBinderFactory {

    public static function create(DoctrineObjectReference $doctrineObjectReference = null):PropertyBinder {
        $annotationDriver = new AnnotationDriver(new AnnotationReader());
        $fileLocator = new FileLocator([]);
        $yamlDriver = new YamlDriver($fileLocator);


        $propertyBinderBuilder = new PropertyBinderBuilder();
        if($doctrineObjectReference !== null) {
            $propertyBinderBuilder->setDoctrineObjectReference($doctrineObjectReference);
        }
        $propertyBinderBuilder
            ->addDriver($annotationDriver)
            ->addDriver($yamlDriver);
        return $propertyBinderBuilder->build();
    }
}