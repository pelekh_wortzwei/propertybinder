<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Handler\Binding;

use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\PropertyBinder;

/**
 * Interface PropertyBindingInterface
 * @package LP\PropertyBinder\Binding
 */
interface PropertyBindingInterface {

    /**
     * @return bool
     */
    public function isCollectionBinding():bool;
    /**
     *
     * @param string $type
     *
     * @return bool
     */
    public function supports(string $type):bool;

    /**
     * @param mixed            $dataToBind
     * @param                  $currentValue
     * @param PropertyMetadata $propertyMetadata
     * @param PropertyBinder   $propertyBinder
     *
     * @return mixed
     */
    public function bind($dataToBind, $currentValue, PropertyMetadata $propertyMetadata, PropertyBinder $propertyBinder);

    /**
     * @return int
     */
    public function getOrder():int;
}