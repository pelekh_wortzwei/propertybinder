<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Handler\Binding;

use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\PropertyBinder;

/**
 * Class SimpleDataBinding
 * @package LP\PropertyBinder\Handler\Binding
 */
class SimpleDataBinding implements PropertyBindingInterface {

    /**
     * @return bool
     */
    public function isCollectionBinding(): bool {
        return false;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function supports(string $type): bool {
        return in_array($type, [
            'int',
            'integer',
            'bool',
            'boolean',
            'string',
            'float',
            'raw'
        ]);
    }

    /**
     * @param                  $dataToBind
     * @param                  $currentValue
     * @param PropertyMetadata $propertyMetadata
     * @param PropertyBinder   $propertyBinder
     *
     * @return mixed
     */
    public function bind($dataToBind, $currentValue, PropertyMetadata $propertyMetadata, PropertyBinder $propertyBinder) {
        if($dataToBind === null) {
            return null;
        }

        if($propertyMetadata->type === 'raw') {
            return $dataToBind;
        }

        settype($dataToBind, $propertyMetadata->type);
        return $dataToBind;
    }

    public function getOrder(): int {
        return 1000;
    }
}