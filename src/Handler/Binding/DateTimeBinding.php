<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Handler\Binding;

use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\PropertyBinder;

/**
 * Class DateTimeBinding
 * @package LP\PropertyBinder\Binding
 */
class DateTimeBinding implements PropertyBindingInterface {

    /**
     * @return bool
     */
    public function isCollectionBinding(): bool {
        return false;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function supports(string $type): bool {
        return class_exists($type) && \DateTime::class == ltrim($type, '\\');
    }

    /**
     * @param mixed            $dataToBind
     * @param                  $currentValue
     * @param PropertyMetadata $propertyMetadata
     * @param PropertyBinder   $propertyBinder
     *
     * @return mixed
     * @throws \Exception
     */
    public function bind($dataToBind, $currentValue, PropertyMetadata $propertyMetadata, PropertyBinder $propertyBinder) {
        if($dataToBind) {
            if(preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $dataToBind)) {
                $dataToBind .= 'T00:00:00';
            }
            return new \DateTime($dataToBind);
        }
        return null;
    }

    public function getOrder(): int {
        return 2000;
    }
}