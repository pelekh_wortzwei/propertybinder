<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Handler\Binding;

use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\PropertyBinder;

/**
 * Class SimpleDataCollectionBinding
 * @package LP\PropertyBinder\Handler\Binding
 */
class SimpleDataCollectionBinding implements PropertyBindingInterface {

    public function isCollectionBinding(): bool {
        return true;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function supports(string $type): bool {
        return in_array($type, [
                'int',
                'integer',
                'bool',
                'boolean',
                'string',
                'float'
            ]);
    }

    /**
     * @param mixed            $dataToBind
     * @param                  $currentValue
     * @param PropertyMetadata $propertyMetadata
     * @param PropertyBinder   $propertyBinder
     *
     * @return mixed
     */
    public function bind($dataToBind, $currentValue, PropertyMetadata $propertyMetadata, PropertyBinder $propertyBinder) {
        if($propertyMetadata->isArrayType()) {
            return $this->bindArray($dataToBind, $propertyMetadata);
        }

        return $this->bindCollection($dataToBind, $currentValue, $propertyMetadata);
    }

    /**
     * @param                  $dataToBind
     *
     * @param PropertyMetadata $propertyMetadata
     *
     * @return array
     */
    private function bindArray($dataToBind, PropertyMetadata $propertyMetadata) {
        if(!is_array($dataToBind)) {
            throw new \LogicException(sprintf('Given data for field "%s" is not an array. Type: %s',
                $propertyMetadata->name,
                gettype($dataToBind)
            ));
        }
        return $dataToBind;
    }

    /**
     * @param                  $dataToBind
     * @param                  $currentValue
     *
     * @param PropertyMetadata $propertyMetadata
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    private function bindCollection($dataToBind, $currentValue, PropertyMetadata $propertyMetadata) {
        $dataToBind = $this->bindArray($dataToBind, $propertyMetadata);
        if($currentValue === null) {
            $currentValue = new \Doctrine\Common\Collections\ArrayCollection($dataToBind);
        }
        $currentValue->clear();
        foreach($dataToBind as $item) {
            $currentValue->add($item);
        }
        return $currentValue;
    }

    public function getOrder(): int {
        return 1000;
    }
}