<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Handler\Binding;

use Doctrine\Persistence\Mapping\ClassMetadata;
use LP\PropertyBinder\Doctrine\DoctrineObjectReference;
use LP\PropertyBinder\Error\PropertyBinderError;
use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\PropertyBinder;

/**
 * Class ObjectBinding
 * @package LP\PropertyBinder\Binding
 */
class ObjectBinding implements PropertyBindingInterface {

    /**
     * @var DoctrineObjectReference
     */
    private $doctrineObjectReference;

    /**
     * @param DoctrineObjectReference $doctrineObjectReference
     */
    public function __construct(DoctrineObjectReference $doctrineObjectReference = null) {
        $this->doctrineObjectReference = $doctrineObjectReference;
    }

    /**
     * @return bool
     */
    public function isCollectionBinding(): bool {
        return false;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function supports(string $type): bool {
        return class_exists($type);
    }

    /**
     * @param mixed            $dataToBind
     * @param                  $currentValue
     * @param PropertyMetadata $propertyMetadata
     * @param PropertyBinder   $propertyBinder
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function bind($dataToBind, $currentValue, PropertyMetadata $propertyMetadata, PropertyBinder $propertyBinder) {
        if($dataToBind === null) {
            return null;
        }

        // Check if the class is an document/entity
        if($this->doctrineObjectReference !== null) {

            $reference = $this->doctrineObjectReference->getReference($propertyMetadata->type, $dataToBind);
            if($reference !== null) {
                return $reference;
            }
        }

        if($currentValue === null) {
            $currentValue = new $propertyMetadata->type;
        }

        if(!is_array($dataToBind)) {
            throw new \InvalidArgumentException(sprintf('Given value for field "%s" is not an array. Type: %s. Needed for an general object',
                $propertyMetadata->name,
                gettype($dataToBind)
            ));
        }

        if(!is_object($currentValue)) {
            throw new \InvalidArgumentException(sprintf('Given current value for field "%s" is not an object. Type: %s',
                $propertyMetadata->name,
                gettype($currentValue)));
        }

        $propertyBinder->bind($currentValue, $dataToBind, $propertyMetadata->groups);
        return $currentValue;
    }

    public function getOrder(): int {
        return 3000;
    }
}