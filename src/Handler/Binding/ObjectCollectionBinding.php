<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Handler\Binding;

use Doctrine\Common\Collections\ArrayCollection;
use LP\PropertyBinder\Doctrine\DoctrineObjectReference;
use LP\PropertyBinder\Error\PropertyBinderError;
use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\PropertyBinder;

/**
 * Class ObjectCollectionBinding
 * @package LP\PropertyBinder\Binding
 */
class ObjectCollectionBinding implements PropertyBindingInterface {

    /**
     * @var DoctrineObjectReference
     */
    private $doctrineObjectReference;

    /**
     * @param DoctrineObjectReference $doctrineObjectReference
     */
    public function __construct(DoctrineObjectReference $doctrineObjectReference = null) {
        $this->doctrineObjectReference = $doctrineObjectReference;
    }


    /**
     * @return bool
     */
    public function isCollectionBinding(): bool {
        return true;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function supports(string $type): bool {
        return class_exists($type);
    }

    /**
     * @param mixed            $dataToBind
     * @param                  $currentValue
     * @param PropertyMetadata $propertyMetadata
     * @param PropertyBinder   $propertyBinder
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function bind($dataToBind, $currentValue, PropertyMetadata $propertyMetadata, PropertyBinder $propertyBinder) {

        if($propertyMetadata->isArrayType()) {
            if($dataToBind === null) {
                if($propertyMetadata->ifNullSetToEmptyCollectionType) {
                    return [];
                }
                return null;
            }
            return $this->bindArray($dataToBind, $propertyMetadata, $propertyBinder);
        }

        if($dataToBind === null) {
            if($propertyMetadata->ifNullSetToEmptyCollectionType) {
                $this->checkCollectionInterface();
                return new ArrayCollection();
            }
            return null;
        }
        return $this->bindCollection($dataToBind, $currentValue, $propertyMetadata, $propertyBinder);
    }

    /**
     * @param array            $values
     * @param PropertyMetadata $propertyMetadata
     * @param PropertyBinder   $propertyBinder
     *
     * @return mixed
     * @throws \ReflectionException
     */
    private function bindArray(array $values, PropertyMetadata $propertyMetadata, PropertyBinder $propertyBinder) {
        $array = [];
        foreach($values as $itemData) {

            $array[] = $this->createObject(
                $propertyMetadata->type,
                $itemData,
                $propertyMetadata->groups,
                $propertyBinder
            );
        }
        return $array;
    }

    /**
     * @param array            $values
     * @param                  $currentValue
     * @param PropertyMetadata $propertyMetadata
     * @param PropertyBinder   $propertyBinder
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     * @throws \ReflectionException
     */
    private function bindCollection(array $values, $currentValue, PropertyMetadata $propertyMetadata, PropertyBinder $propertyBinder) {
        $this->checkCollectionInterface();

        if($currentValue === null) {
            $currentValue = new \Doctrine\Common\Collections\ArrayCollection();
        }

        $currentValue->clear();

        if($this->doctrineObjectReference !== null) {
            foreach ($values as $itemData) {
                if ($itemData === null) continue;

                $obj = $this->doctrineObjectReference->getReference($propertyMetadata->type, $itemData);
                if ($obj === null) {
                    $obj = $this->createObject(
                        $propertyMetadata->type,
                        $itemData,
                        $propertyMetadata->groups,
                        $propertyBinder
                    );
                }
                $currentValue->add($obj);
            }
            return $currentValue;
        }


        foreach($values as $itemData) {
            $obj = $this->createObject(
                $propertyMetadata->type,
                $itemData,
                $propertyMetadata->groups,
                $propertyBinder
            );
            $currentValue->add($obj);
        }
        return $currentValue;
    }

    /**
     * @param string         $type
     * @param                $objectData
     * @param array          $groups
     * @param PropertyBinder $propertyBinder
     *
     * @return mixed
     * @throws \ReflectionException
     */
    protected function createObject(string $type, array $objectData, array $groups, PropertyBinder $propertyBinder) {
        $obj = new $type;
        $propertyBinder->bind($obj, $objectData, $groups);
        return $obj;
    }

    private function checkCollectionInterface() {
        $interface = 'Doctrine\Common\Collections\Collection';
        if(!interface_exists($interface)) {
            throw new PropertyBinderError(sprintf('
                    Collection type is "Collection" but the interface "%s" was not found.
                    Please be sure to install the Doctrine Collections if you want to use collections',
                $interface
            ));
        }
    }

    public function getOrder(): int {
        return 2000;
    }
}