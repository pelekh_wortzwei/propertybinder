<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Handler;

use LP\PropertyBinder\Handler\Binding\PropertyBindingInterface;

/**
 * Class PropertyBindingRegistry
 * @package LP\PropertyBinder\Handler
 */
class PropertyBindingRegistry {

    const SINGLE_BINDING_KEY = 'single';
    const COLLECTION_BINDING_KEY = 'collection';

    /**
     * @var array
     */
    private $bindings = [];

    public function __construct() {
        $this->bindings = [];
    }

    /**
     * @param PropertyBindingInterface $propertyBinding
     *
     * @return PropertyBindingRegistry
     */
    public function register(PropertyBindingInterface $propertyBinding): self {
        $this->bindings[$this->getKey($propertyBinding->isCollectionBinding())][] = $propertyBinding;
        return $this;
    }

    /**
     * @param bool $isCollection
     *
     * @return array
     */
    public function getPropertyBindings(bool $isCollection):array {
        $key = $this->getKey($isCollection);
        if(array_key_exists($key, $this->bindings)) {
            return $this->bindings[$this->getKey($isCollection)];
        }
        return [];
    }

    public function sortBindings():void {
        uasort($this->bindings[self::SINGLE_BINDING_KEY], function (PropertyBindingInterface $a, PropertyBindingInterface $b) {
            return $a->getOrder() > $b->getOrder();
        });

        uasort($this->bindings[self::COLLECTION_BINDING_KEY], function (PropertyBindingInterface $a, PropertyBindingInterface $b) {
            return $a->getOrder() > $b->getOrder();
        });
    }

    /**
     * @param bool $isCollection
     *
     * @return string
     */
    private function getKey(bool $isCollection):string {
        return $isCollection ? self::COLLECTION_BINDING_KEY : self::SINGLE_BINDING_KEY;
    }
}