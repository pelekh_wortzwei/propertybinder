<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Annotation;

/**
 * Class Annotation
 * @package LP\PropertyBinder\Annotation
 */
abstract class Annotation {
    public function __construct(array $data) {

        foreach($data as $key => $item) {
            if(property_exists($this, $key)) {
                $this->{$key} = $item;
            }
        }

        foreach($this->getRequired() as $requiredProperty) {
            if(property_exists($this, $requiredProperty) && $this->$requiredProperty === null) {
                throw new \InvalidArgumentException(sprintf('Property "%s" is required and cannot be NULL in %s',
                    $requiredProperty,
                    get_class($this)
                ));
            }
        }

        foreach($this->getAvailableValues() as $key => $availableValue) {
            if(!property_exists($this, $key)) continue;

            if(is_array($this->$key)) {
                if(count(array_intersect($availableValue, $this->$key)) === 0) {
                    throw new \InvalidArgumentException(sprintf(
                        'Property %s must have one of the following values: %s . Values given: %s',
                        $key,
                        implode(',', $availableValue),
                        implode(',', $this->$key)
                    ));
                }
            } else {
                if(!in_array($this->$key, $availableValue)) {
                    throw new \InvalidArgumentException(sprintf(
                        'Property %s must have one of the following values: %s . Value given: %s',
                        $key,
                        implode(',', $availableValue),
                        $this->$key
                    ));
                }
            }
        }
    }

    protected function getRequired():array {
        return [];
    }

    protected function getAvailableValues():array {
        return [];
    }
}