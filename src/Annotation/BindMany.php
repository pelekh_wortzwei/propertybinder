<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Annotation;
use LP\PropertyBinder\Metadata\PropertyMetadata;

/**
 * @Annotation
 * Class BindMany
 * @package LP\PropertyBinder\Annotation
 */
final class BindMany extends Bind {

    /**
     * array or collection
     * @var string
     */
    public $collectionType;

    /**
     * if the value is null, but an array is expected
     * set the value to an empty array or collection
     * @var bool
     */
    public $ifNullSetToEmptyCollectionType;

    protected function getRequired(): array {
        return array_merge(parent::getRequired(), ['collectionType']);
    }

    protected function getAvailableValues(): array {
        return array_merge_recursive(parent::getAvailableValues(), [
            'collectionType' => [PropertyMetadata::ARRAY, PropertyMetadata::COLLECTION]
        ]);
    }
}