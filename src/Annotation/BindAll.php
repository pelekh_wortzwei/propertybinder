<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Annotation;

/**
 * Class BindAll
 * @package LP\PropertyBinder\Annotation
 * @Annotation
 */
final class BindAll extends Annotation {

    /**
     * @var string
     */
    public $field;

    /**
     * @var bool
     */
    public $useSetter = false;

    /**
     * @var string
     */
    public $setterName;

    /**
     * @var array
     */
    public $groups = [];
}