<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Annotation;
/**
 * Class Bind
 * @package LP\PropertyBinder\Annotation
 */
abstract class Bind extends Annotation {
    /**
     * @var array
     */
    public $groups = [];

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $arrayKey;

    /**
     * @var string
     */
    public $setterName;

    /**
     * @var bool
     */
    public $useSetter = false;

    /**
     * @var string
     */
    public $getterName;

    /**
     * @var bool
     */
    public $useGetter = false;

    protected function getRequired(): array {
        return [
            'type'
        ];
    }

}