<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Annotation;
/**
 * @Annotation
 * Class BindOne
 * @package LP\PropertyBinder\Annotation
 */
final class BindOne extends Bind {

}