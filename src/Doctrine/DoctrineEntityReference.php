<?php

/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Doctrine;


use Doctrine\ORM\EntityManager;
use LP\PropertyBinder\Error\PropertyBinderError;

/**
 * Class DoctrineEntityReference
 * @package LP\PropertyBinder\Doctrine
 */
class DoctrineEntityReference extends AbstractObjectReference {

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $class
     * @param mixed  $dataToBind
     *
     * @return object|null
     */
    public function getReference(string $class, $dataToBind) {
        try {
            $classMetadata = $this->entityManager->getClassMetadata($class);
            $identifierFieldName = $this->getIdentifierFieldName($classMetadata);
            $identityFieldType = $this->getIdentifierFieldType($classMetadata, $identifierFieldName);
            $referenceValue = $this->getReferenceValue($dataToBind, $identifierFieldName, $identityFieldType);
            if($referenceValue !== null) {
                if(!$this->entityExists($class, $identifierFieldName, $referenceValue)) {
                    throw new PropertyBinderError(sprintf('Cant find an entity class %s [%s:%s] to reference the object',
                        $classMetadata->getName(),
                        $identifierFieldName,
                        $referenceValue
                    ));
                }
                return $this->entityManager->getReference($class, $referenceValue);
            }
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * check if the object with id really exists
     *
     * @param string $class
     * @param string $identifierFieldName
     * @param string $id
     *
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function entityExists(string $class, string $identifierFieldName, $id): bool {
        $qb = $this->entityManager->createQueryBuilder();
        $qb
            ->select($qb->expr()->count('c'))
            ->from($class, 'c')
            ->where('c.' . $identifierFieldName . ' = ?1')
            ->setParameter(1, $id);

        return $qb->getQuery()->getSingleScalarResult() !== 0;
    }
}