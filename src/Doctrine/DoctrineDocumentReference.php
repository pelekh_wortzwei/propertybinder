<?php

/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Doctrine;

use Doctrine\ODM\MongoDB\DocumentManager;
use LP\PropertyBinder\Error\PropertyBinderError;

/**
 * Class DoctrineDocumentReference
 * @package LP\PropertyBinder\Doctrine
 */
class DoctrineDocumentReference extends AbstractObjectReference {

    /**
     * @var DocumentManager
     */
    public $documentManager;

    /**
     * @param DocumentManager $documentManager
     */
    public function __construct(DocumentManager $documentManager) {
        $this->documentManager = $documentManager;
    }

    /**
     * @param string $class
     * @param mixed  $dataToBind
     *
     * @return object|null
     */
    public function getReference(string $class, $dataToBind) {
        try {
            $classMetadata = $this->documentManager->getClassMetadata($class);
            if($classMetadata->isEmbeddedDocument) {
                return null;
            }

            $identifierFieldName = $this->getIdentifierFieldName($classMetadata);
            $referenceValue = $this->getReferenceValue($dataToBind, $identifierFieldName, 'string');
            if($referenceValue !== null) {
                if(!$this->entityExists($class, $identifierFieldName, $referenceValue)) {
                    throw new PropertyBinderError(sprintf('Cant find an entity class %s [%s:%s] to reference the object',
                        $classMetadata->getName(),
                        $identifierFieldName,
                        $referenceValue
                    ));
                }
                // Check for inheritance
                if($classMetadata->inheritanceType !== \Doctrine\ODM\MongoDB\Mapping\ClassMetadata::INHERITANCE_TYPE_NONE) {
                    // get the whole object cause we dont know the discrimination type here
                    return $this->documentManager->find($class, $referenceValue);
                }
                // give a reference back
                return $this->documentManager->getReference($class, $referenceValue);
            }
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * check if the object with id really exists
     *
     * @param string $class
     * @param string $fieldName
     * @param        $id
     *
     * @return bool
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    private function entityExists(string $class, string $fieldName, $id): bool {
        $qb = $this->documentManager->createQueryBuilder($class);
        $qb->field($fieldName)->equals($id);
        return $qb->count()->getQuery()->execute() !== 0;
    }
}