<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Doctrine;

use Doctrine\Persistence\Mapping\ClassMetadata;

/**
 * Class AbstractObjectReference
 * @package LP\PropertyBinder\Doctrine
 */
abstract class AbstractObjectReference implements DoctrineObjectReference {

    /**
     * @param ClassMetadata $classMetadata
     *
     * @return string
     * @throws \Exception
     */
    public function getIdentifierFieldName(ClassMetadata $classMetadata):string {
        $identifierFieldNames = $classMetadata->getIdentifier();
        if(count($identifierFieldNames) !== 1) {
            throw new \Exception(sprintf('The entity "%s" has no identifier or multiple identifier. Needed exaclty one identifier',
                $classMetadata->getName()
            ));
        }
        return $identifierFieldNames[0];
    }

    /**
     * @param ClassMetadata $classMetadata
     *
     * @param string        $identifierFieldName
     *
     * @return string
     * @throws \Exception
     */
    public function getIdentifierFieldType(ClassMetadata $classMetadata, string $identifierFieldName):string {
        $identityFieldType = $classMetadata->getTypeOfField($identifierFieldName);
        if($identityFieldType === null) {
            throw new \Exception(sprintf('Cant find type for field "%s" in class "%s"',
                    $identifierFieldName,
                    $classMetadata->getName())
            );
        }
        return $identityFieldType;
    }

    /**
     * @param        $dataToBind
     * @param string $identifierFieldName
     * @param string $identifierFieldType
     *
     * @return mixed|null
     */
    public function getReferenceValue($dataToBind, string $identifierFieldName, string $identifierFieldType) {
        if(is_array($dataToBind) && array_key_exists($identifierFieldName, $dataToBind)) {
            if(gettype($dataToBind[$identifierFieldName]) === $identifierFieldType) {
                return $dataToBind[$identifierFieldName];
            }
        }

        if(gettype($dataToBind) === $identifierFieldType) {
            return $dataToBind;
        }
        return null;
    }
}