<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Doctrine;

/**
 * Interface DoctrineObjectReference
 * @package LP\PropertyBinder\Doctrine
 */
interface DoctrineObjectReference {

    /**
     * get the doctrine reference
     *
     * @param string $class
     * @param mixed $dataToBind
     *
     * @return object
     */
    public function getReference(string $class, $dataToBind);
}