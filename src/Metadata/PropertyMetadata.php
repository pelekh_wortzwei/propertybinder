<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Metadata;
use Metadata\PropertyMetadata as BasePropertyMetadata;

/**
 * Class PropertyMetadata
 * @package LP\PropertyBinder\Metadata
 */
class PropertyMetadata extends BasePropertyMetadata {

    /**
     * @var string
     */
    const ARRAY = 'array';

    /**
     * @var string
     */
    const COLLECTION = 'collection';

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $arrayKey;

    /**
     * @var array
     */
    public $groups = [];

    /**
     * @var string
     */
    public $collectionType;

    /**
     * @var bool
     */
    public $ifNullSetToEmptyCollectionType;

    /**
     * @var bool
     */
    public $useSetter;

    /**
     * @var string
     */
    public $setterName;

    /**
     * @var bool
     */
    public $useGetter;

    /**
     * @var string
     */
    public $getterName;

    /**
     * @var \ReflectionProperty
     */
    private $reflection;

    /**
     * @internal
     *
     * @var bool
     */
    public $forceReflectionAccess = false;

    public function __construct(string $class, string $name) {
        parent::__construct($class, $name);

        try {
            $class = $this->getReflection()->getDeclaringClass();
            $this->forceReflectionAccess = $class->isInternal() || $class->getProperty($name)->isStatic();
        } catch (\ReflectionException $e) {
        }
    }


    public function getValue($obj) {

        if(!$this->useGetter) {
            $reflection = $this->getReflection();

            if(PHP_VERSION_ID >= 70400) {
                return $obj !== null && $reflection->isInitialized($obj) ? $this->getReflection()->getValue($obj) : null;
            }

            return $this->getReflection()->getValue($obj);
        }

        $getterMethod = $this->getterName !== null ? $this->getterName : 'get' . ucfirst($this->name);
        return $obj->$getterMethod();
    }

    public function setValue($obj, $value) {

        if(!$this->useSetter) {
            $this->getReflection()->setValue($obj, $value);
        } else {
            $setterName = $this->setterName !== null ? $this->setterName : 'set' . ucfirst($this->name);
            if(method_exists($obj, $setterName)) {
                $obj->$setterName($value);
            } else {
                throw new \InvalidArgumentException(sprintf('Cant find method "%s" in Class "%s"',
                    $setterName,
                    get_class($obj)
                ));
            }
        }
    }

    /**
     * @return bool
     */
    public function isSingleType():bool {
        return $this->collectionType === null;
    }

    /**
     * @return bool
     */
    public function isArrayType():bool {
        return $this->collectionType === self::ARRAY;
    }

    /**
     * @return bool
     */
    public function isCollectionType():bool {
        return $this->collectionType === self::COLLECTION;
    }

    /**
     * @return bool
     */
    public function hasCollectionBinding():bool {
        return $this->collectionType !== null;
    }

    /**
     * @return string
     */
    public function getArrayKeyName():string {
        return $this->arrayKey !== null ?  $this->arrayKey : $this->name;
    }

    public function serialize() {
        return serialize([
            $this->type,
            $this->arrayKey,
            $this->groups,
            $this->collectionType,
            $this->ifNullSetToEmptyCollectionType,
            $this->useSetter,
            $this->setterName,
            $this->useGetter,
            $this->getterName,
            $this->forceReflectionAccess,
            parent::serialize()
        ]);
    }

    public function unserialize($str) {
        $unserialized = unserialize($str);

        [
            $this->type,
            $this->arrayKey,
            $this->groups,
            $this->collectionType,
            $this->ifNullSetToEmptyCollectionType,
            $this->useSetter,
            $this->setterName,
            $this->useGetter,
            $this->getterName,
            $this->forceReflectionAccess,
            $parentStr
        ] = $unserialized;
        parent::unserialize($parentStr);
    }

    private function getReflection(): \ReflectionProperty {
        if($this->reflection === null) {
            $this->reflection = new \ReflectionProperty($this->class, $this->name);
            $this->reflection->setAccessible(true);
        }
        return $this->reflection;
    }
}