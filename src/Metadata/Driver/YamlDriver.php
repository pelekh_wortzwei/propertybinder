<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Metadata\Driver;

use LP\PropertyBinder\Error\InvalidMetadataError;
use LP\PropertyBinder\Metadata\PropertyMetadata;
use Metadata\ClassMetadata;
use Metadata\Driver\AbstractFileDriver;
use Symfony\Component\Yaml\Yaml;

/**
 * Class YamlDriver
 * @package LP\PropertyBinder\Metadata\Driver
 */
class YamlDriver extends AbstractFileDriver {

    /**
     * Parses the content of the file, and converts it to the desired metadata.
     *
     * @param \ReflectionClass $class
     * @param string           $file
     *
     * @return \Metadata\ClassMetadata|null
     */
    protected function loadMetadataFromFile(\ReflectionClass $class, string $file): ?ClassMetadata {
        $config = Yaml::parse(file_get_contents($file));

        if (!isset($config[$class->name])) {
            throw new InvalidMetadataError(sprintf('Expected metadata for class %s to be defined in %s.', $class->name, $file));
        }

        if(!isset($config[$class->name]['properties'])) {
            throw new InvalidMetadataError(sprintf('Missing field "properties" under %s in file %s',
                $class->name,
                $file
            ));
        }

        $metadata = new ClassMetadata($class->name);
        foreach ($class->getProperties() as $property) {
            if($property->class !== $class->name || (isset($property->info) && $property->info['class'] !== $class->name)) continue;

            if(!array_key_exists($property->name, $config[$class->name]['properties'])) continue;

            $propertyMetadata = new PropertyMetadata($class->name, $property->name);
            $propertyMetadataConfig = $config[$class->name]['properties'][$property->name];

            $this->assertRequiredField($propertyMetadataConfig, 'type', $property->name, $class->name, $file);
            $propertyMetadata->type = $propertyMetadataConfig['type'];

            $this
                ->addFieldToPropertyMetadata($propertyMetadata, $propertyMetadataConfig, 'groups', [])
                ->addFieldToPropertyMetadata($propertyMetadata, $propertyMetadataConfig, 'arrayKey')
                ->addFieldToPropertyMetadata($propertyMetadata, $propertyMetadataConfig, 'setterName')
                ->addFieldToPropertyMetadata($propertyMetadata, $propertyMetadataConfig, 'useSetter', false)
                ->addFieldToPropertyMetadata($propertyMetadata, $propertyMetadataConfig, 'getterName')
                ->addFieldToPropertyMetadata($propertyMetadata, $propertyMetadataConfig, 'useGetter', false)
                ->addFieldToPropertyMetadata($propertyMetadata, $propertyMetadataConfig, 'collectionType')
                ->addFieldToPropertyMetadata($propertyMetadata, $propertyMetadataConfig, 'ifNullSetToEmptyCollectionType');
            $metadata->addPropertyMetadata($propertyMetadata);
        }
        return $metadata;
    }

    /**
     * @param PropertyMetadata $propertyMetadata
     * @param array            $propertyMetaData
     * @param string           $key
     *
     * @param null             $defaultIfNotFound
     *
     * @return YamlDriver
     */
    private function addFieldToPropertyMetadata(PropertyMetadata $propertyMetadata, array $propertyMetaData,
                                                        string $key, $defaultIfNotFound = null): self {

        $value = array_key_exists($key, $propertyMetaData) ? $propertyMetaData[$key] : $defaultIfNotFound;
        $propertyMetadata->$key = $value;
        return $this;
    }

    /**
     * Returns the extension of the file.
     *
     * @return string
     */
    protected function getExtension(): string {
        return 'yml';
    }

    /**
     * @param array  $propertyMetadataConfig
     * @param string $key
     * @param string $propertyName
     * @param string $class
     * @param string $file
     */
    private function assertRequiredField(array $propertyMetadataConfig, string $key, string $propertyName, string $class, string $file) {
        if(!array_key_exists($key, $propertyMetadataConfig)) {
            throw new InvalidMetadataError(sprintf('Key %s is missing under %s in class %s in file %s',
                $key,
                $propertyName,
                $class,
                $file
            ));
        }
    }
}