<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Metadata\Driver;

use Metadata\Driver\DriverInterface;

/**
 * Interface DriverFactoryInterface
 * @package LP\PropertyBinder\Metadata\Driver
 */
interface DriverFactoryInterface {

    /**
     * @return DriverInterface
     */
    public function createDriver(): DriverInterface;
}