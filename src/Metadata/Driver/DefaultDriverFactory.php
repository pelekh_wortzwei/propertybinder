<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Metadata\Driver;

use Doctrine\Common\Annotations\Reader;
use Metadata\Driver\DriverChain;
use Metadata\Driver\DriverInterface;
use Metadata\Driver\FileLocator;

/**
 * Class DefaultDriverFactory
 * @package LP\PropertyBinder\Metadata\Driver
 */
final class DefaultDriverFactory implements DriverFactoryInterface {

    /**
     * @var Reader
     */
    private $annotationReader;

    /**
     * @var array
     */
    private $metadataDirs;

    public function __construct(Reader $annotationReader, array $metadataDirs = []) {
        $this->annotationReader = $annotationReader;
        $this->metadataDirs = $metadataDirs;
    }

    /**
     * @param array  $metadataDirs
     * @param Reader $annotationReader
     *
     * @return DriverInterface
     */
    public function createDriver(): DriverInterface {
        $driverChain = new DriverChain();
        $driverChain->addDriver(new AnnotationDriver($this->annotationReader));

        if(!empty($this->metadataDirs)) {
            $fileLocator = new FileLocator($this->metadataDirs);
            $driverChain->addDriver(new YamlDriver($fileLocator));
        }
        return $driverChain;
    }

    /**
     * @param DriverInterface $driver
     */
    public function addDriver(DriverInterface $driver):void {
        $this->drivers[] = $driver;
    }
}