<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Metadata\Driver;

use Doctrine\Common\Annotations\Reader;
use LP\PropertyBinder\Annotation\Bind;
use LP\PropertyBinder\Annotation\BindAll;
use LP\PropertyBinder\Annotation\BindMany;
use LP\PropertyBinder\Annotation\BindOne;
use LP\PropertyBinder\Error\PropertyBinderError;
use LP\PropertyBinder\Metadata\PropertyMetadata;
use LP\PropertyBinder\Metadata\ClassMetadata;
use Metadata\Driver\DriverInterface;

/**
 * Class AnnotationDriver
 * @package LP\PropertyBinder\Metadata\Driver
 */
final class AnnotationDriver implements DriverInterface {

    /**
     * @var Reader
     */
    private $reader;

    /**
     * @param Reader $reader
     */
    public function __construct(Reader $reader) {
        $this->reader = $reader;
    }

    /**
     * @param \ReflectionClass $class
     *
     */
    public function loadMetadataForClass(\ReflectionClass $class): ?\Metadata\ClassMetadata {
        $classMetadata = new ClassMetadata($class->name);

        // Annotation on class
        /** @var BindAll $bindAllAnnotation */
        $bindAllAnnotation = $this->reader->getClassAnnotation($class, BindAll::class);
        if($bindAllAnnotation !== null) {
            if($bindAllAnnotation->field !== null || $bindAllAnnotation->useSetter) {
                $this->assertBindAllAnnotation($bindAllAnnotation);

                $classMetadata->bindAll = true;
                $classMetadata->field = $bindAllAnnotation->field;
                $classMetadata->setterName = $bindAllAnnotation->setterName;
                $classMetadata->useSetter = $bindAllAnnotation->useSetter;
                $classMetadata->groups = $bindAllAnnotation->groups;
            } else {
                $classMetadata->bindAll = false;
            }
        }

        // Annotation on properties
        foreach($class->getProperties() as $property) {
            if($property->class !== $class->name || (isset($property->info) && $property->info['class'] !== $class->name)) continue;

            $annotations = $this->reader->getPropertyAnnotations($property);
            $bindingAnnotations = array_filter($annotations, function($annotation) {
                return $annotation instanceof Bind;
            });

            // No binding annotation was set on property
            if(count($bindingAnnotations) === 0) continue;

            $this->assertValidAnnotations($bindingAnnotations);

            $propertyMetadata = new PropertyMetadata($class->name, $property->getName());
            foreach($bindingAnnotations as $annotation) {
                if(!($annotation instanceof Bind)) continue;

                if($annotation instanceof Bind) {
                    $propertyMetadata->type = $annotation->type;
                    $propertyMetadata->groups = $annotation->groups;
                    $propertyMetadata->arrayKey = $annotation->arrayKey;
                    $propertyMetadata->useSetter = $annotation->useSetter;
                    $propertyMetadata->setterName = $annotation->setterName;
                    $propertyMetadata->useGetter = $annotation->useGetter;
                    $propertyMetadata->getterName = $annotation->getterName;
                }

                if($annotation instanceof BindOne) {
                } else if($annotation instanceof BindMany) {
                    $propertyMetadata->collectionType = $annotation->collectionType;
                    $propertyMetadata->ifNullSetToEmptyCollectionType = $annotation->ifNullSetToEmptyCollectionType;
                }
            }

            $classMetadata->addPropertyMetadata($propertyMetadata);
        }
        return $classMetadata;
    }

    /**
     * @param BindAll $bindAll
     */
    private function assertBindAllAnnotation(BindAll $bindAll) {
        if($bindAll->useSetter && $bindAll->field === null && $bindAll->setterName === null) {
            throw new PropertyBinderError(sprintf('use setter is set in the bindAll annotation on class %s, but a field or setterName isnt.'));
        }
    }

    /**
     * @param array $annotations
     */
    private function assertValidAnnotations(array $annotations):void {

        if(count(array_filter($annotations, function($annotation) {
            return $annotation instanceof BindOne || $annotation instanceof BindMany;
        })) >= 2) {
            throw new PropertyBinderError(sprintf(
                'BindOne and BindMany are both set as Annotation but only one can be set.'
            ));
        };
    }
}