<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
declare(strict_types=1);
namespace LP\PropertyBinder\Metadata;
use Metadata\ClassMetadata as BaseClassMetadata;

class ClassMetadata extends BaseClassMetadata {

    /**
     * @var bool
     */
    public $bindAll;

    /**
     * @var string
     */
    public $field;

    /**
     * @var bool
     */
    public $useSetter;

    /**
     * @var string
     */
    public $setterName;

    /**
     * @var array
     */
    public $groups;

    /**
     * @var \ReflectionClass
     */
    private $reflection;

    public function bindAll($object, array $data) {
        $reflection = $this->getReflection();
        if($this->field !== null && !$this->useSetter) {
            $propertyReflection = $reflection->getProperty($this->field);
            $propertyReflection->setAccessible(true);
            $propertyReflection->setValue($object, $data);
            return;
        }

        $method = null;
        if($this->useSetter && $this->field !== null) {
            $method = 'set' . ucfirst($this->field);
        } else if($this->useSetter && $this->setterName !== null) {
            $method = $this->setterName;
        }
        if($method !== null) {
            if(!$reflection->hasMethod($method)) {
                throw new \InvalidArgumentException(sprintf('Cant find method "%s" in Class "%s"',
                    $method,
                    get_class($object)
                ));
            }
            $object->$method($data);
        }
    }

    public function serialize() {
        return serialize([
            $this->bindAll,
            $this->field,
            $this->useSetter,
            $this->setterName,
            $this->groups,
            parent::serialize()
        ]);
    }

    public function unserialize($str) {
        $unserialized = unserialize($str);

        [
            $this->bindAll,
            $this->field,
            $this->useSetter,
            $this->setterName,
            $this->groups,
            $parentStr
        ] = $unserialized;
        parent::unserialize($parentStr);
    }

    private function getReflection(): \ReflectionClass
    {
        if($this->reflection === null) {
            $this->reflection = new \ReflectionClass($this->name);
        }
        return $this->reflection;
    }
}