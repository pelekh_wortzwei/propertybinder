<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */

namespace LP\PropertyBinder;

use InvalidArgumentException;
use LP\PropertyBinder\Doctrine\DoctrineObjectReference;
use LP\PropertyBinder\Handler\Binding\DateTimeBinding;
use LP\PropertyBinder\Handler\Binding\ObjectBinding;
use LP\PropertyBinder\Handler\Binding\ObjectCollectionBinding;
use LP\PropertyBinder\Handler\Binding\SimpleDataBinding;
use LP\PropertyBinder\Handler\Binding\SimpleDataCollectionBinding;
use LP\PropertyBinder\Handler\PropertyBindingRegistry;

use Metadata\Cache\CacheInterface;
use Metadata\Cache\FileCache;
use Metadata\Driver\DriverChain;
use Metadata\Driver\DriverInterface;
use Metadata\MetadataFactory;
use RuntimeException;

/**
 * Class PropertyBinderBuilder
 * @package LP\PropertyBinder
 */
class PropertyBinderBuilder {

    /**
     * @var PropertyBindingRegistry
     */
    private $propertyBindingRegistry;

    /**
     * @var array
     */
    private $drivers = [];

    /**
     * @var CacheInterface
     */
    private $metadataCache;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var DoctrineObjectReference
     */
    private $doctrineObjectReference;

    /**
     * @param string $dir
     *
     * @return PropertyBinderBuilder
     */
    public function setCacheDir(string $dir): self {
        if (!is_dir($dir)) {
            $this->createDir($dir);
        }
        if (!is_writable($dir)) {
            throw new InvalidArgumentException(sprintf('The cache directory "%s" is not writable.', $dir));
        }

        $this->cacheDir = $dir;
        return $this;
    }

    /**
     * @param DoctrineObjectReference $doctrineObjectReference
     *
     * @return self
     */
    public function setDoctrineObjectReference(DoctrineObjectReference $doctrineObjectReference):self {
        $this->doctrineObjectReference = $doctrineObjectReference;
        return $this;
    }

    /**
     * @param DriverInterface $driver
     *
     * @return $this
     */
    public function addDriver(DriverInterface $driver):self {
        $this->drivers[] = $driver;
        return $this;
    }

    /**
     * @param PropertyBindingRegistry $propertyBindingRegistry
     *
     * @return PropertyBinderBuilder
     */
    public function setPropertyBindingRegistry(PropertyBindingRegistry $propertyBindingRegistry): self {
        $this->propertyBindingRegistry = $propertyBindingRegistry;
    }

    /**
     * @param CacheInterface $metadataCache
     *
     * @return PropertyBinderBuilder
     */
    public function setMetadataCache(CacheInterface $metadataCache):self {
        $this->metadataCache = $metadataCache;
        return $this;
    }

    /**
     * @return PropertyBinder
     */
    public function build():PropertyBinder {

        if($this->propertyBindingRegistry === null) {
            $this->propertyBindingRegistry = new PropertyBindingRegistry();
            $this->registerDefaultPropertyBindings();
        }
        $this->propertyBindingRegistry->sortBindings();

        $metadataFactory = new MetadataFactory(new DriverChain($this->drivers));

        if ($this->metadataCache !== null) {
            $metadataFactory->setCache($this->metadataCache);
        } elseif ($this->cacheDir !== null) {
            $this->createDir($this->cacheDir . '/metadata');
            $metadataFactory->setCache(new FileCache($this->cacheDir . '/metadata'));
        }
        return new PropertyBinder($this->propertyBindingRegistry, $metadataFactory);
    }

    /**
     * @param string $dir
     */
    private function createDir(string $dir): void
    {
        if (is_dir($dir)) {
            return;
        }

        if (@mkdir($dir, 0777, true) === false && is_dir($dir) === false) {
            throw new RuntimeException(sprintf('Could not create directory "%s".', $dir));
        }
    }

    /**
     * @return PropertyBinderBuilder
     */
    private function registerDefaultPropertyBindings():self {
        $this->propertyBindingRegistry
            ->register(new SimpleDataBinding())
            ->register(new DateTimeBinding())
            ->register(new ObjectBinding($this->doctrineObjectReference))
            ->register(new SimpleDataCollectionBinding())
            ->register(new ObjectCollectionBinding($this->doctrineObjectReference));
        return $this;
    }
}