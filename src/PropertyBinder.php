<?php
declare(strict_types=1);
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */
namespace LP\PropertyBinder;

use LP\PropertyBinder\Error\PropertyBinderError;
use LP\PropertyBinder\Handler\Binding\PropertyBindingInterface;
use LP\PropertyBinder\Handler\PropertyBindingRegistry;
use LP\PropertyBinder\Metadata\ClassMetadata;
use LP\PropertyBinder\Metadata\PropertyMetadata;
use Metadata\MetadataFactory;

/**
 * Class PropertyBinder
 * @package LP\PropertyBinder
 */
class PropertyBinder {

    /**
     * @var PropertyBindingRegistry
     */
    private $propertyBindingRegistry;

    /**
     * @var MetadataFactory
     */
    private $metadataFactory;

    /**
     * @param PropertyBindingRegistry $propertyBindingRegistry
     * @param MetadataFactory         $metadataFactory
     */
    public function __construct(PropertyBindingRegistry $propertyBindingRegistry, MetadataFactory $metadataFactory) {
        $this->propertyBindingRegistry = $propertyBindingRegistry;
        $this->metadataFactory = $metadataFactory;
    }

    /**
     * @param       $object
     * @param array $objectData
     *
     * @param array $groups
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function bind($object, array $objectData, array $groups = []) {
        if(!is_object($object)) {
            throw new \InvalidArgumentException(sprintf('Object in "%s" expected. Got "%s"', __METHOD__, gettype($object)));
        }

        if(empty($groups)) {
            $groups[] = 'default';
        }

        $metadata = $this->metadataFactory->getMetadataForClass(get_class($object));
        if($metadata === null) {
            throw new \RuntimeException(sprintf(
                'Cant find metadata for class %s. Did you forget to include the annotation or yml driver?',
                get_class($object))
            );
        }
        /** @var ClassMetadata $classMetadata */
        foreach($metadata->classMetadata as $classMetadata) {
            //TODO: Maybe make it generic like the propertyMetadata
            if($classMetadata->bindAll) {
                if(empty($classMetadata->groups)) {
                    $classMetadata->groups[] = 'default';
                }
                if(count(array_intersect($groups, $classMetadata->groups)) === 0) {
                    continue;
                }
                $object = $this->bindAll($classMetadata, $object, $objectData);
                continue;
            }

            /** @var PropertyMetadata $propertyMetadata */
            foreach($classMetadata->propertyMetadata as $propertyMetadata) {
                // Group has a "default" group by default if there are no other groups set on this property
                if(empty($propertyMetadata->groups)) {
                    $propertyMetadata->groups[] = 'default';
                }
                if(count(array_intersect($groups, $propertyMetadata->groups)) === 0) {
                    continue;
                }
                $this->setValue($propertyMetadata, $object, $objectData);
            }
        }
        return $object;
    }

    /**
     * @param PropertyMetadata    $propertyMetadata
     * @param                     $object
     * @param array               $objectData
     */
    private function setValue(PropertyMetadata $propertyMetadata, $object, array $objectData):void {
        if(!array_key_exists($propertyMetadata->getArrayKeyName(), $objectData)) return;

        $currentValue = $propertyMetadata->getValue($object);
        $value = $this->bindProperty($propertyMetadata, $currentValue, $objectData[$propertyMetadata->getArrayKeyName()]);
        $propertyMetadata->setValue($object, $value);
    }

    /**
     * @param PropertyMetadata $propertyMetadata
     * @param mixed            $currentValue
     * @param mixed            $dataToBind
     *
     * @return mixed
     */
    private function bindProperty(PropertyMetadata $propertyMetadata, $currentValue, $dataToBind) {

        /** @var PropertyBindingInterface $binding */
        foreach($this->propertyBindingRegistry->getPropertyBindings($propertyMetadata->hasCollectionBinding()) as $binding) {
            if($binding->supports($propertyMetadata->type)) {
                return $binding->bind($dataToBind, $currentValue, $propertyMetadata, $this);
            }
        }

        throw new PropertyBinderError(sprintf(
            'Cant find transformer for type "%s". You can write a custom transformer if needed',
            $propertyMetadata->type
        ));
    }

    /**
     * @param ClassMetadata $classMetadata
     * @param               $object
     * @param array         $objectData
     *
     * @return bool
     */
    private function bindAll(ClassMetadata $classMetadata, $object, array $objectData) {
        if($object === null) {
            $object = new $classMetadata->name;
        }
        $classMetadata->bindAll($object, $objectData);
        return $object;
    }
}