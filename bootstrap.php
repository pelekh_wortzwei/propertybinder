<?php
/**
 * Copyright (c) 2019. René Möllmer <rene@moellmer.net>
 */

require_once('vendor/autoload.php');

// Autoloader sample for a PHP framework
spl_autoload_register(function ($className) {
    echo $className;
    $file = __DIR__ . "/src/{$className}.php";
    if (file_exists($file)) {
        require_once($file);
    }
});
